Categories:Internet
License:MIT
Web Site:
Source Code:https://github.com/Nutomic/syncthing-android
Issue Tracker:https://github.com/Nutomic/syncthing-android/issues
Donate:https://tip4commit.com/github/Nutomic/syncthing-android

Auto Name:Syncthing
Summary:File synchronization
Description:
This is the first attempt at an Android port of syncthing. Please understand
that this is not stable yet, and read the first start dialog carefully.

Syncthing replaces proprietary sync and cloud services with something open,
trustworthy and decentralized. Your data is your data alone and you deserve to
choose where it is stored, if it is shared with some third party and how it's
transmitted over the Internet.
.

Repo Type:git
Repo:https://github.com/Nutomic/syncthing-android.git

Build:0.4.8,22
    commit=80072194b94e0ca0f81e5bc3b88c8f88867e2e5c
    gradle=fat

Build:0.4.9,23
    commit=0.4.9
    gradle=fat

Build:0.5.0-beta2,24
    commit=0.5.0-beta2
    gradle=fat

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.5.0-beta2
Current Version Code:24

