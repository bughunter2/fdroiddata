AntiFeatures:UpstreamNonFree
Categories:Navigation
License:Apache2
Web Site:http://www.cgeo.org
Source Code:https://github.com/cgeo/c-geo-opensource
Issue Tracker:https://github.com/cgeo/c-geo-opensource/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=cgeo%40cgeo%2eorg&item_name=Donate%20to%20CGeo%20via%20F-droid

Auto Name:c:geo
Summary:Geocaching client
Description:
Geocaching is a sport where people hide things and you go and find them. This
is an unnofficial client to access geocaching.com's list of caches, search and
save them to the device.

Older builds of this application no longer work due to reliance on third party
services that have changed. Newer builds cannot be built without proprietary
software, and in any case have proprietary dependencies and 'secret' keys missing
from the source code.
.

Repo Type:git
Repo:https://github.com/cgeo/c-geo-opensource.git

Build:2012.06.18,20120618
    disable=No longer works due to 3rd party service changes
    commit=388a5a3b1c0885a
    subdir=main
    forceversion=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:15

Build:2012.08.15,20120815
    disable=No corresponding source
    commit=unknown - see disabled

Build:2012.12.03,20121203
    disable=No longer works due to 3rd party service changes
    commit=market_20121203
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.01.10-fdroid,20130110
    disable=No longer works due to 3rd party service changes
    commit=market_20130110
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.02.07-fdroid,20130207
    disable=No longer works due to 3rd party service changes
    commit=market_20130207
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.02.21-fdroid,20130221
    disable=No longer works due to 3rd party service changes
    commit=market_20130221
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.04.03-fdroid,20130403
    disable=No longer works due to 3rd party service changes
    commit=market_20130403
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.05.13-fdroid,20130513
    disable=No longer works due to 3rd party service changes
    commit=market_20130513
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.05.16-fdroid,20130516
    disable=No longer works due to 3rd party service changes
    commit=market_20130516
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.05.18-fdroid,20130518
    disable=No such tag market_20130518
    commit=unknown - see disabled
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.05.22-fdroid,20130522
    disable=No longer works due to 3rd party service changes
    commit=market_20130522
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml
    target=Google Inc.:Google APIs:16

Build:2013.05.31a-fdroid,20130532
    disable=No longer works due to 3rd party service changes
    commit=market_20130532
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.06.05-fdroid,20130605
    disable=No longer works due to 3rd party service changes
    commit=market_20130605
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.07.25-fdroid,20130725
    disable=No longer works due to 3rd party service changes
    commit=market_20130725
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Auto Update Mode:None
Update Check Mode:Static
Current Version:2013.07.25-fdroid
Current Version Code:20130725

